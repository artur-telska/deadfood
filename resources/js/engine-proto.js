var player = {
    x: 0,
    y: 140,
    width: 20,
    height: 20,
    step: 10
};

var enemy = {
    x: 0,
    y: 0,
    width: 20,
    height: 20,
    step: 8
};

var world = {
    level: 0,
    height: 300,
    width: 300,
    enemies: [],
    ctx: document.getElementById('canvas').getContext('2d'),
    
    loadEnemies: function() {
        for (var i = 0; i <= this.level; i++) {
            this.enemies[i] = Object.create(enemy);
            this.enemies[i].x = (Math.floor(Math.random() * 100)) + 200;
            this.enemies[i].y = (this.height/this.level * (i * 0.93)); 
        }
    },
    drawEnemies: function() {        
        this.ctx.fillStyle = 'black';
        for (enemy of this.enemies) {
            this.ctx.fillRect(enemy.x, enemy.y, enemy.height, enemy.width);
        }
    },
    drawPlayer: function() {
        this.ctx.fillStyle = 'orange';
        this.ctx.fillRect(player.x, player.y, player.height, player.width);
    },
    processMove: function(key) {
        var x = player.x;
        var y = player.y;
        var s = player.step;
                
        switch(key) {
            case 65:
                x -= s;
                break;
            case 68:
                x += s;
                break;
            case 83:
                y += s;
                break;
            case 87:
                y -= s;
                break;                   
        }

        if (y < 0 || y > (this.height - player.height) || x < 0) {
            
        } else if(x > (this.width - player.width)) {
            this.newLevel();
        } else {
            player.x = x;
            player.y = y;

            if (this.isBitten()) {
                console.log('GAME OVER');
            }

            this.ctx.clearRect(0, 0, this.width, this.height);
            this.moveEnemies();
            this.drawPlayer();
            this.drawEnemies();
        }

    },
    gameOver: function() {
        
        player.x = 0;
        player.y = 0;
    },
    newLevel: function() {  
       player.x = 0;
       this.level++;
       this.loadEnemies();
       this.ctx.clearRect(0, 0, this.width, this.height);
       this.drawPlayer();
       this.drawEnemies();        
    },
    isBitten: function() {
        var pX1 = player.x;
        var pX2 = player.x + player.width;
        var pY1 = player.y;
        var pY2 = player.y + player.height;

        for (enemy of this.enemies) {
            var eX1 = enemy.x;
            var eX2 = enemy.x + enemy.width;
            var eY1 = enemy.y;
            var eY2 = enemy.y + enemy.height;

            if ((pX1 <= eX1 && eX1 <= pX2 || pX1 <= eX2 && eX2 <= pX2) &&
                (pY1 <= eY1 && eY1 <= pY2 || pY1 <= eY2 && eY2 <= pY2)) {
                return true;
            }
        }
    },
    moveEnemies: function() {
        var pX = player.x;
        var pY = player.y;

        for (enemy of this.enemies) {
            var eX = enemy.x;
            var eY = enemy.y;
            var eS = enemy.step;
            
            pX > eX ? enemy.x += Math.round(Math.random() * eS) : enemy.x -= Math.round(Math.random() * eS);
            pY > eY ? enemy.y += Math.round(Math.random() * eS) : enemy.y -= Math.round(Math.random() * eS);

        }
    } 
};

world.newLevel();

document.addEventListener('keydown', function(event){

    world.processMove(event.keyCode);

});