document.addEventListener("DOMContentLoaded", function(event) {
    
    var playerDefaultImage = new Image();
    playerDefaultImage.src = '/deadfood/resources/img/player-right.png';
    
    var enemyDefaultImage = new Image();
    enemyDefaultImage.src = '/deadfood/resources/img/zombie-left.png';    
    
    // player object - holds player params and position 
    var player = {
        x: 0,
        y: 140,
        width: 22,
        height: 31,
        step: 10,
        image: playerDefaultImage
    };
    // enemy object - holds enemy params and position
    var enemy = {
        x: 0,
        y: 0,
        width: 23,
        height: 33,
        step: 8,
        image: enemyDefaultImage
    };
    
    // game world object - holds game world params and methods
    var world = {
        level: 0,
        height: document.getElementById('canvas').height,
        width: document.getElementById('canvas').width,
        enemies: [],
        ctx: document.getElementById('canvas').getContext('2d'),
        
        // load enemies according to level and set default values
        loadEnemies: function() {
            this.enemies = [];
            for (var i = 0; i <= this.level; i++) {
                this.enemies[i] = Object.create(enemy);
                this.enemies[i].x = this.width - (Math.floor(Math.random() * 100));
                this.enemies[i].y = (this.height/this.level * (i * 0.93));     
            }
        },
        
        // draw enemies according to position
        drawEnemies: function() {        
            this.ctx.fillStyle = 'black';
            for (enemy of this.enemies) {
                this.ctx.drawImage(enemy.image, enemy.x, enemy.y);
            }
        },
        
        // draw player according to position
        drawPlayer: function() {
            this.ctx.fillStyle = 'orange';
            this.ctx.drawImage(player.image, player.x, player.y);
        },
        
        // processing moves, switch course and check contact between player and enemy
        processMove: function(key) {
            var x = player.x;
            var y = player.y;
            var s = player.step;
                    
            switch(key) {
                case 65:
                    x -= s;
                    player.image.src = '/deadfood/resources/img/player-left.png';
                    break;
                case 68:
                    x += s;
                    player.image.src = '/deadfood/resources/img/player-right.png';
                    break;
                case 83:
                    y += s;
                    break;
                case 87:
                    y -= s;
                    break;                   
            }
    
            if (y < 0 || y > (this.height - player.height) || x < 0) {
                
            } else if(x > (this.width - player.width)) {
                this.newLevel();
            } else {
                player.x = x;
                player.y = y;
    
                if (this.isBitten()) {
                    alert("Game Over!");
                    this.level = 0;
                    this.newLevel();
                } else {
                
                    this.ctx.clearRect(0, 0, this.width, this.height);
                    this.moveEnemies();
                    this.drawPlayer();
                    this.drawEnemies();                   
                    
                }
    
            }
    
        },
        
        // start new level, clear canvas, load and draw parties
        newLevel: function() {  
           document.getElementById('level').innerHTML = "Level " + this.level;
           player.x = 0;
           this.level++;
           this.loadEnemies();
           this.ctx.clearRect(0, 0, this.width, this.height);
           this.drawPlayer();
           this.drawEnemies();  
           console.log('Level loaded', player.image);      
        },
        
        // return true if player is bitten by enemy
        isBitten: function() {
            var pX1 = player.x;
            var pX2 = player.x + player.width;
            var pY1 = player.y;
            var pY2 = player.y + player.height;
    
            for (enemy of this.enemies) {
                var eX1 = enemy.x;
                var eX2 = enemy.x + enemy.width;
                var eY1 = enemy.y;
                var eY2 = enemy.y + enemy.height;
    
                if ((pX1 <= eX1 && eX1 <= pX2 || pX1 <= eX2 && eX2 <= pX2) &&
                    (pY1 <= eY1 && eY1 <= pY2 || pY1 <= eY2 && eY2 <= pY2)) {
                    return true;
                }
            }
        },
        
        // move enemy according to player moves
        moveEnemies: function() {
            var pX = player.x;
            var pY = player.y;
    
            for (enemy of this.enemies) {
                var eX = enemy.x;
                var eY = enemy.y;
                var eS = enemy.step;
                
                if (pX > eX) {
                    enemy.x += Math.round(Math.random() * eS);
                    enemy.image.src = '/deadfood/resources/img/zombie-right.png';
                } else {
                    enemy.x -= Math.round(Math.random() * eS);
                    enemy.image.src = '/deadfood/resources/img/zombie-left.png';
                }
                if (pY > eY) {
                    enemy.y += Math.round(Math.random() * eS);
                   
                } else {
                    enemy.y -= Math.round(Math.random() * eS);
                }
            }
        } 
    };
     
    document.addEventListener('keydown', function(event){
    
        world.processMove(event.keyCode);
    
    });
    
    world.newLevel();
});